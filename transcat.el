;;; transcat.el --- Segmentation-based translation  -*- lexical-binding: t; -*-

;; Author: Marty Hiatt <martianhiatus AT riseup.net>
;; Copyright (C) 2022 Marty Hiatt <martianhiatus AT riseup.net>
;; Package-Requires: ((emacs "28.1"))
;; Package-Version: 0.1
;; Homepage: https://codeberg.org/martianh/transcat
;; Keywords: convenience, translation, languages

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Segment-based-translation.
;; Work with XLF and TMX files in Emacs.
;; For now, projects are assumed to be structured as OmegaT projects.

;;; Code:

(require 'xml)
(require 'nxml-parse)
(require 'dom)
(require 'cl-lib)
(require 'sentex nil :no-error)
(require 'transcat-segment)
(require 'transcat-hl-segment)
(require 'transcat-langs)

(defgroup transcat nil
  "Transcat translation package."
  :group 'transcat)

(defface transcat-source-segment-face
  '((t (:inherit 'variable-pitch)))
  "Face for source segment text.")

(defface transcat-target-segment-face
  '((t (:inherit 'variable-pitch)))
  "Face for target segment text.")

(defcustom transcat-project-type 'plain-text
  "Type of project, for directory structure."
  :type '(choice (const :tag "Plain text" plain-text)
                 (const :tag "OmegaT" omegat)
                 (const :tag "Okapi Framework" okapi)))

(defcustom transcat-project-view 'two-up
  "Default type of buffer view.
Interleaved means display alternating source and target segments.
Two-up means display source and target in separate buffers."
  :type '(choice (const :tag "Side by side" two-up)
                 (const :tag "Interleaved" interleaved)))

(defcustom transcat-use-sentex t
  "Whether to use `sentex.el' for segmenting source text.
Segment provides sophisticated sentence-ending rules."
  :type 'boolean)

;; emacs defaults, to avoid user config by default
;; obtained with (car (get 'sentence-end-base 'standard-value))
;; maybe sentence-end emacs default could be optional?
(defcustom transcat-sentence-end-base "[.?!…‽][]\"'”’)}»›]*"
  "Regex matching end of a sentence, not including space.
Our version of `sentence-end-base'.
Defaults to Emacs' default value, not user customized value."
  :type 'string)

(defcustom transcat-make-source-read-only t
  "Whether a source buffer should be read only."
  :type 'boolean)

(defcustom transcat-highlight-current-source-segment t
  "Whether the current segment should be highlighted in the source buffer."
  :type 'boolean)

(defcustom transcat-highlight-current-target-segment t
  "Whether the current segment should be highlighted in the source buffer."
  :type 'boolean)

(defvar-local transcat-project-name "")

(defvar-local transcat-project-root nil)

(defvar transcat-project-source-dir (concat transcat-project-root
                                            "/source/"))

(defvar transcat-project-target-dir (concat transcat-project-root
                                            "/target/"))

(defvar-local transcat-project-source-language-code nil)

(defvar-local transcat-project-target-language-code nil)

(defvar transcat-project-source-file nil)

(defvar transcat-project-target-file nil)

(defvar transcat-source-buffer nil)

(defvar transcat-target-buffer nil)

(defvar transcat-source-window nil)

(defvar-local transcat-last-completed-segment nil)

(defvar-local transcat-current-working-segment nil)


(defvar transcat-source-codes-list
  (cond ((eq sentex-ruleset-framework 'omegat)
         transcat-languages-alist-omegat)
        ((eq sentex-ruleset-framework 'okapi)
         (append transcat-languages-alist-okapi-plain
                 transcat-languages-alist-okapi-alt))
        ((eq sentex-ruleset-framework 'okapi-icu4j)
         transcat-languages-alist-okapi-icu4j)))
;; '("en-GB" "en-US" "fr" "de" "pt-PT" "pt-BR"))

(defvar transcat-forward-sentence-function
  (if transcat-use-sentex
      'sentex-forward-sentence
    'forward-sentence)
  "Function to move forward by one sentence. Used in segmentation.")

(defvar transcat-backward-sentence-function
  (if transcat-use-sentex
      'sentex-backward-sentence
    'backward-sentence)
  "Function to move backward by one sentence. Used in segmentation.")


;;; SIMPLE NAVIGATION CMDS
;; we need both segment nav funs, with buffer sync
;; but we also need non-sync nav funs, or at least
;; to handle our separators possibly interrupting existing sentence nav

(defun transcat--move-past-separator-maybe ()
  "Move past `transcat-separator-string' if point is before it."
  (when (looking-at transcat-separator-string)
    (forward-char (length transcat-separator-string))))

(defun transcat--move-past-separator-maybe-backwards ()
  "Move backwards past `transcat-separator-string' if point is after it."
  (when (looking-back transcat-separator-string)
    (backward-char (length transcat-separator-string))))

(defun transcat-forward-to-next-sentence ()
  "Move forward to the beginning of the next sentence.
If `transcat-separator-string' is found, move past it."
  ;; we might have sentences in our segments
  (interactive)
  (transcat--forward-sentence)
  (forward-whitespace 1)
  ;; move past poss paragraph break:
  (while (looking-at "[ \t]+\\|\n")
    (forward-whitespace 1))
  (transcat--move-past-separator-maybe))

(defun transcat-backward-to-sentence-begin ()
  "Move back to the beginning of the current sentence.
If point is just after `transcat-separator-string', move past it
and then move to beginning of previous sentence."
  (interactive)
  (transcat--move-past-separator-maybe-backwards)
  (transcat--backward-sentence)
  (transcat--move-past-separator-maybe))

(defun transcat-forward-segment (&optional arg)
  "Move point to the beginning of the next segment.
Do so ARG times. If ARG is negative, move to previous segment ARG times.
Point is placed after any separator."
  (interactive "p")
  (unless (re-search-forward transcat-separator-string nil t arg)
    (message "No next segment?"))
  (transcat--move-past-separator-maybe)
  (point))

(defun transcat-backward-segment (&optional arg)
  "Move to beginning of current or previous segment ARG times.
Point is placed after any separator."
  (interactive "p")
  ;; if we are at begin of seg, backward past separator:
  (transcat--move-past-separator-maybe-backwards)
  ;; otherwise, we only backward to beginning of current
  (transcat-forward-segment (- (or arg 1)))
  (transcat--move-past-separator-maybe)
  (point))

(defun transcat-previous-segment ()
  "Move to beginning of previous segment.
Point is placed after any separator."
  (interactive)
  (if (not (looking-back transcat-separator-string))
      ;; not at seg begin, so go back twice:
      (transcat-forward-segment -2)
    ;; else at seg begin, so backward past separator:
    (transcat--move-past-separator-maybe-backwards)
    ;; and back to prev seg begin
    (transcat-forward-segment -1)
    (transcat--move-past-separator-maybe))
  (point))

(defun transcat--goto-segment-by-id (id)
  "Jump to segment with ID."
  (interactive "nGo to segment number: ")
  (goto-char (point-min))
  (let ((id-prop-range (text-property-search-forward 'id id t)))
    (if (not id-prop-range)
        (message "Looks like buffer not segmented?")
      (goto-char (prop-match-beginning id-prop-range))
      (transcat--move-past-separator-maybe))))

(defun transcat-jump-to-current-working-segment (&optional last-segment)
  "Jump to the current working segment.
Or to LAST-SEGMENT if given."
  (interactive)
  (transcat--goto-segment-by-id (or last-segment ; for sync call
                                    transcat-last-completed-segment))
  (transcat-forward-segment)
  (transcat-hl-segment-current))


;;; NAVIGATION SYNC CMDS
(defun transcat-forward-segment-sync (&optional arg)
  "Move point to the beginning of the next segment and sync source.
Do so ARG times. If ARG is negative, move to previous segment ARG times."
  (interactive "p")
  (if (and (not (transcat--get-prop-for-segment 'id))
           (not (save-excursion
                  (re-search-forward transcat-separator-string nil t))))
      (message "No next segment?")
    (let ((before-id (transcat--get-prop-for-segment 'id)))
      (transcat-forward-segment arg)
      (let ((seg-id (or (transcat--get-prop-for-segment 'id)
                        ;; if destination has no id yet because in progress:
                        (+ arg before-id))))
        (transcat--sync-source-by-id seg-id)
        (message "Segment %s" seg-id)))))

(defun transcat-backward-segment-sync (&optional arg)
"Move to beginning of current or previous segment ARG times and sync source."
(interactive "p")
(transcat-backward-segment arg)
(transcat--move-past-separator-maybe)
(let ((seg-id (transcat--get-prop-for-segment 'id)))
  (transcat--sync-source-by-id seg-id)
  (message "Segment %s" seg-id)))

(defun transcat-sync-source ()
  "Sync source buffer to current target segment.
Try to sync by ID, and if that fails try to sync by current sentence."
  (interactive)
  ;; maybe current seg has no id yet:
  (let ((before-id (save-excursion
                     (transcat-previous-segment)
                     (transcat--get-prop-for-segment 'id))))
    (transcat--sync-source-by-id (1+ before-id))))

(defun transcat--sync-source-by-id (id)
  "Move point in source buffer to beginning of segment with ID."
  (with-selected-window transcat-source-window
    (transcat--goto-segment-by-id id)
    (transcat-hl-segment-current)))

(defun transcat--sync-source-by-string-maybe ()
  "Try to sync source buffer based on segment or sentence at point."
  ;; TODO: try sync by looking for current segment-sentence in struct?
  ;; won't work without source inserted in target buffer
  ;; but what would work, is get previous segment source and 1+ sync that
  )

(defun transcat-jump-to-current-working-segment-sync ()
  "Jump to the current working segment in source and target buffers."
  (interactive)
  (let ((last-seg transcat-last-completed-segment)
        (jump-to-fun transcat-jump-to-current-working-segment))
    (transcat--sync-call jump-to-fun last-seg)))
    ;; (transcat-jump-to-current-working-segment last-seg)
    ;; (with-selected-window transcat-source-window
    ;; (transcat-jump-to-current-working-segment last-seg)
    ;; (transcat-hl-segment-current))))

(defun transcat-recenter-top-bottom-sync (&optional arg)
  "Recenter source and target buffers using `recenter-top-bottom'.
ARG is the argument to pass to `recenter-top-bottom' in each window."
  (interactive)
  (recenter-top-bottom arg)
  (transcat-sync-source)
  (with-selected-window transcat-source-window
    ;; make `recenter-last-op' local so that it has the same value after
    ;; being called in each window
    (make-local-variable 'recenter-last-op)
    (recenter-top-bottom arg)))

(defun transcat-scroll-down-sync ()
  "Scroll downward in both buffers."
  (interactive)
  (let ((scroll-down-fun (if cua-mode 'cua-scroll-down 'scroll-down)))
    (transcat--sync-call scroll-down-fun)))

(defun transcat-scroll-up-sync ()
  "Scroll up in both buffers."
  (interactive)
  (let ((scroll-up-fun (if cua-mode 'cua-scroll-up 'scroll-up)))
    (transcat--sync-call scroll-up-fun)))

(defun transcat--sync-call (fun &optional args)
  "Call function FUN with ARGS in current buffer then in source buffer."
  (call-interactively fun)
  (with-selected-window transcat-source-window
    (apply fun (list args))
    (transcat-hl-segment-current)))


;;; MANAGING TEXT PROPERTIES

(defun transcat--get-source-prop-for-segment (prop id)
  "Return value of text PROP for source segment with ID."
  (with-current-buffer transcat-source-buffer
    (save-excursion (transcat--goto-segment-by-id id)
                    (transcat--get-prop-for-segment
                     prop))))

(defun transcat--get-prop-for-segment (prop)
  "Get text PROP for current segment."
  (get-text-property (point) prop))

(defun transcat-insert-source-segment-in-target ()
  "Insert source text in target buffer."
  ;; only needed if we don't populate target with source
  (interactive)
  (let* ((struct (transcat--get-prop-for-segment 'struct))
         (source-segment (transcat-segment-pair-source-text struct)))
    (insert source-segment)))

(defun transcat--add-source-props-to-target-segment (target-id)
  "Add text properties for segment with id TARGET-ID.
Text properties are fetched from source segment with same ID."
  ;; is this duplication necessary or just more mess?
  (save-excursion
    (if (= target-id 1)
        (transcat-backward-to-sentence-begin) ; no prev separator
      (transcat-backward-segment))
    (let ((source-struct
           (transcat--get-source-prop-for-segment 'struct target-id))
          (source-seg-id
           (transcat--get-source-prop-for-segment 'id target-id))
          ;; we're at begin of seg:
          (seg-begin (point))
          (seg-end (progn
                     (transcat-forward-segment)
                     (transcat--move-past-separator-maybe-backwards)
                     (point))))
      (add-text-properties seg-begin
                           seg-end
                           (list 'id source-seg-id
                                 'struct source-struct)))))

;; TODO: handle new-segment called when already in a series of segs
(defun transcat-new-segment ()
  "Start a new translation segment.
Used in the translation text when a segment is complete, to start
the next one."
  (interactive)
  (if (or (looking-at transcat-separator-string)
          (save-excursion
            (backward-char (length transcat-separator-string))
            (looking-at transcat-separator-string)))
      (message "Looks like we already have a separator here?")
    (let* ((prev-seg-id (transcat--get-prev-target-segment-id))
           (before-sep-id (1+ prev-seg-id))
           (after-sep-id (1+ before-sep-id)))
      (insert transcat-separator-string)
      ;; add text properties to before-sep-seg
      (if (= before-sep-id 1)
          ;; add props to first target segment in buffer:
          (save-excursion
            ;; backward-segment unavail here, we are in first segment:
            (transcat-backward-to-sentence-begin)
            (insert transcat-separator-string)
            (transcat--add-source-props-to-target-segment before-sep-id))
        ;; or just to prev segment:
        (transcat--add-source-props-to-target-segment before-sep-id))
      ;; TODO: save previous seg to struct
      ;; (transcat-save-target-segment-to-struct))
      (transcat--sync-source-by-id after-sep-id)
      (message "Working on segment %s" after-sep-id)
      (setq-local transcat-last-completed-segment before-sep-id)
      (setq-local transcat-current-working-segment after-sep-id)
      (when transcat-highlight-current-target-segment
        (transcat-hl-segment-current)))))

(defun transcat--get-prev-target-segment-id ()
  "Return the ID of the last completed target segment.
If there is no previous separator, return 0."
  ;; we haven't inserted any separator yet:
  (if (not
       (save-excursion
         (re-search-backward transcat-separator-string nil t)))
      0
    (save-excursion
      ;; however we do the movement robustly:
      (transcat-backward-segment) ; back to begin of current
      (transcat--move-past-separator-maybe-backwards)
      (backward-char 2)
      (transcat--get-prop-for-segment 'id))))

(defun transcat-save-target-segment-to-struct ()
  "Save target segment text to its struct."
  (let* ((seg-beg (point))
         (seg-end (save-excursion (sentex-forward-sentence)
                                  (point)))
         (segment-string-no-properties
          (buffer-substring seg-beg seg-end))
         (struct (transcat--get-prop-for-segment 'struct)))
    (setf (transcat-segment-pair-target-text struct)
          segment-string)))

(defun transcat-new-segment-at-sentence-end ()
  "Start a new segment at end of current sentence."
  (interactive)
  (transcat-forward-to-next-sentence)
  (transcat-new-segment))


;;; FIND FILES + BUFFER SETUP
(defun transcat-get-two-letter-lang-codes (alist)
  "Map ALIST with CADR."
  (mapcar #'cadr alist))

(defun transcat-project-find-source ()
  "Find source file for current project."
  (interactive)
  (let ((source-file
         (read-file-name "Open transcat source file: "
                         (or transcat-project-source-dir
                             default-directory))))
    (setq transcat-project-source-file (expand-file-name source-file))
    (with-current-buffer (find-file source-file)
      (transcat-src-mode 1)
      ;; set targ lang here so we can add to struct:
      (unless transcat-project-target-language-code
        (setq transcat-project-target-language-code
              (completing-read "Two-letter target language code: "
                               (transcat-get-two-letter-lang-codes
                                transcat-source-codes-list)
                               nil
                               'confirm)))
      ;; seg if not segged (but also need to load from file if poss):
      (unless (re-search-forward transcat-separator-string nil t)
        (when (y-or-n-p "Segment buffer now? ")
          (transcat-segment-source-buffer)))
      (setq transcat-source-window (selected-window))
      (setq transcat-source-buffer (buffer-name (current-buffer))))))

(defun transcat-project-find-target ()
  "Find target file for current project."
  (interactive)
  (let ((target-file
         (read-file-name "Open target file: "
                         (or transcat-project-target-dir
                             default-directory)
                         nil
                         nil)))
    (setq transcat-project-target-file (expand-file-name target-file))
    (with-current-buffer (find-file target-file)
      (setq transcat-target-buffer (buffer-name (current-buffer)))
      (transcat-mode))))

(defun transcat-find-files-and-setup-buffers ()
  ""
  (interactive)
  (delete-other-windows)
  (transcat-project-find-target)
  (split-window-right)
  (windmove-right)
  (set-window-buffer (selected-window) (transcat-project-find-source))
  (other-window 1))


;;; WRITING TO DISK
(defun transcat--write-structs-to-file ()
  ""
  (with-temp-file (concat transcat-project-root
                          transcat-project-source-file
                          "-structs.el")
    (let ((print-length nil)
          (standard-output (current-buffer)))
      (insert
       (pp transcat-source-segment-structs)))))


;;; KEYMAPS AND MODES
;; for now its based on `org-translate-mode-map'
(defvar transcat-mode-map
  (let ((map (make-sparse-keymap)))
    (define-key map (kbd "C-M-f") #'transcat-forward-segment-sync)
    (define-key map (kbd "C-M-b") #'transcat-backward-segment-sync)
    (define-key map (kbd "C-M-n") #'transcat-new-segment)
    ;; (define-key map (kbd "C-M-t") #'ogt-update-source-location)
    ;; (define-key map (kbd "C-M-y") #'ogt-new-glossary-term)
    ;; (define-key map (kbd "C-M-;") #'ogt-insert-glossary-translation)
    map))

(define-minor-mode transcat-src-mode
  "Transcat source minor mode."
  :init-value nil
  :lighter " trcat-src"
  :keymap transcat-mode-map ;; TODO: keymap will diverge eventually
  (when transcat-make-source-read-only
    (read-only-mode 1))
  (when transcat-highlight-current-source-segment
    (transcat-hl-segment-mode 1)))

(define-minor-mode transcat-mode
  "Transcat minor mode."
  :init-value nil
  :lighter " trcat"
  (when transcat-highlight-current-target-segment
    (transcat-hl-segment-mode 1)))

(provide 'transcat)
;;; transcat.el ends here
