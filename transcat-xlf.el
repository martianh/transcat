;;; transcat-xlf.el --- XLF functions for transcat   -*- lexical-binding: t; -*-

;; Author: Marty Hiatt <martianhiatus AT riseup.net>
;; Copyright (C) 2022 Marty Hiatt <martianhiatus AT riseup.net>
;; Package-Requires: ((emacs "28.1"))
;; Package-Version: 0.1
;; Homepage: https://codeberg.org/martianh/transcat
;; Keywords: convenience, translation, languages

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; 

;;; Code:

(require 'xml)
(require 'nxml-parse)

;;; XLF FILES:
;; build segment pair structs from XLF file:
;; NB: Okapi's segmentation treats newlines as segment breaks by default! Urgh!

(defun transcat--get-segment-from-xlf (dom)
  "Get a segment pair from parsed XLF file DOM."
  (transcat-segment-pair-create
   :id (dom-attr dom 'id)
   :source-lang (dom-attr (dom-child-by-tag dom 'source)
                          'xml:lang)
   :source-text (dom-text (dom-by-tag dom 'source))
   :target-lang (dom-attr (dom-child-by-tag dom 'target)
                          'xml:lang)
   :target-text (transcat--get-segment-xlf-target-text dom)))

(defun transcat--get-segment-xlf-target-text (dom)
  "Get target text from parsed XLF file DOM."
  ;; text is actually in a sub tag "mrk", w attrs mid and mtype
  (dom-texts (dom-by-tag dom 'target)))

(defun transcat--map-segments-from-xlf (parsed-xlf-body)
  "Return a list of segment pairs from PARSED-XLF-BODY.
Each segment pair is a struct."
  (let ((segments (dom-by-tag parsed-xlf-body 'trans-unit)))
    (mapcar (lambda (x)
              (transcat--get-segment-from-xlf x))
            segments)))

;; TODO: xlf header info
;; (defun transcat--get-xlf-header-info (parsed-xlf)
;;   "")

(defun transcat--convert-xlf-file (file)
  "Convert all segment pairs from FILE.
Returns a list containing a struct for each segment pair."
  (let* ((parsed-xlf-file (transcat--parse-xml-file file))
         (xlf-body (dom-by-tag parsed-xlf-file 'body)))
    ;; (transcat--get-xlf-header-info parsed-xlf-file)
    (transcat--map-segments-from-xlf xlf-body)))


;;; printing XLF buffers:
(defun transcat--print-single-segment (seg-pair &optional target)
  "Print and propertize a segment from SEG-PAIR.
TARGET means the segment is in the target language."
  (let* ((id (transcat-segment-pair-id seg-pair))
         (source-lang (transcat-segment-pair-source-lang seg-pair))
         (source-text (transcat-segment-pair-source-text seg-pair))
         (target-lang (transcat-segment-pair-target-lang seg-pair))
         (target-text (transcat-segment-pair-target-text seg-pair)))
    (insert
     (propertize (if target
                     target-text
                   source-text)
                 ;; TODO: maybe just add struct data as prop
                 ;; make it work same as plain text segs
                 'id id
                 'struct seg-pair
                 'type (if target
                           'target
                         'source)
                 'source-lang source-lang
                 'source-text source-text
                 'target-lang target-lang
                 'target-text target-text
                 'readonly (unless target t)))))

;; printing interleaved view
(defun transcat-print-segmented-xlf-text-interleaved (file)
  "Print segmented interleaved source/target text in new transcat buffer.
FILE is the XLF file to be converted and printed."
  (interactive)
  (let* ((xlf-data (transcat--convert-xlf-file file))
         ;; (body (plist-get xlf-data :body))
         (inhibit-read-only t))
    (with-current-buffer (get-buffer-create
                          (concat "Trcat-int: "
                                  transcat-project-name))
      (switch-to-buffer (current-buffer))
      (erase-buffer)
      (transcat--print-segmented-xlf-source-target-interleaved xlf-data))))

(defun transcat--print-segmented-xlf-source-target-interleaved (seg-pairs)
  "Print segment pairs SEG-PAIRS."
  (mapc (lambda (x)
          ;; just super rough as proof of idea:
          (insert transcat-separator-string
                  "source seg:\n")
          (transcat--print-single-segment x)
          (insert transcat-separator-string
                  "target seg:\n")
          (transcat--print-single-segment x :target))
        seg-pairs))

;; printing separate source / target buffers
(defun transcat--find-segmented-xlf-source-or-target-buffer (file &optional target)
  ""
  (let* ((xlf-data (transcat--convert-xlf-file file))
         ;; (body (plist-get tmx-data :body))
         (inhibit-read-only t))
    (with-current-buffer (get-buffer-create
                          (concat (format "Trcat-%s: "
                                          (if target
                                              "targ"
                                            "src"))
                                  transcat-project-name))
      (switch-to-buffer (current-buffer))
      (erase-buffer)
      (if target
          (transcat--print-segmented-xlf-source-or-target xlf-data :target)
        (transcat-src-mode)
        (transcat--print-segmented-xlf-source-or-target xlf-data)))))

(defun transcat-find-segmented-xlf-source-buffer (file)
  ""
  (interactive)
  (transcat--find-segmented-xlf-source-or-target-buffer file))

(defun transcat-find-segmented-xlf-target-buffer (file)
  ""
  (interactive)
  (transcat--find-segmented-xlf-source-or-target-buffer file :target))

(defun transcat--print-segmented-xlf-source-or-target (seg-pairs &optional target)
  ""
  (let ((inhibit-read-only t))
    (mapc (lambda (x)
            ;; just super rough as proof of idea:
            (insert transcat-separator-string
                    "source seg:\n")
            (transcat--print-single-segment x target))
          seg-pairs)))


(provide 'transcat-xlf)
;;; transcat-xlf.el ends here
