;;; transcat-hl-segment.el --- Highlight a sentence based on customizable face  -*- lexical-binding: t; -*-

;; Copyright (c) 2011 Donald Ephraim Curtis
;; Author: Donald Ephraim Curtis <dcurtis@milkbox.net>
;; Package-Requires: ((emacs "28.1"))
;; Package-Version: 0.1
;; Homepage: https://codeberg.org/martianh/transcat
;; Keywords: convenience, translation, languages

;; This file is not part of GNU Emacs.

;;; License:

;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation; either version 3
;; of the License, or (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to the
;; Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
;; Boston, MA 02110-1301, USA.

;;; Commentary:
;;
;; Highlight the current segment using `transcat-hl-segment' face.
;; code adapted from http://github.com/milkypostman/hl-sentence, but we use `sentex.el' to find segements.

;;; Code:
(require 'sentex)

(defgroup transcat-hl-segment nil
  "Highlight the current sentence."
  :group 'convenience)

(defvar-local transcat-hl-segment-extent nil
  "The location of the transcat-hl-segment-mode overlay.")

;;;###autoload
(defface transcat-hl-segment '((t :inherit highlight))
  "The face used to highlight the current sentence."
  :group 'transcat)

(defun transcat-hl-segment-begin-pos ()
  "Return the point of the beginning of a sentence."
  (save-excursion
    (unless (= (point) (point-max))
      (forward-char))
    (sentex-backward-sentence)
    (point)))

(defun transcat-hl-segment-end-pos ()
  "Return the point of the end of a sentence."
  (save-excursion
    (unless (= (point) (point-max))
      (forward-char))
    (sentex-backward-sentence)
    (sentex-forward-sentence)
    (point)))

;;;###autoload
(define-minor-mode transcat-hl-segment-mode
  "Enable highlighting of current sentence."
  :init-value nil
  (if transcat-hl-segment-mode
      (progn
        (add-hook 'post-command-hook 'transcat-hl-segment-current nil t)
        (setq-local transcat-hl-segment-extent (make-overlay 0 0 (current-buffer)))
        (overlay-put transcat-hl-segment-extent 'face 'transcat-hl-segment))
    (move-overlay transcat-hl-segment-extent 0 0 (current-buffer))
    (remove-hook 'post-command-hook 'transcat-hl-segment-current t)))

(defun transcat-hl-segment-current ()
  "Highlight current sentence."
  (and transcat-hl-segment-mode (> (buffer-size) 0)
       (boundp 'transcat-hl-segment-extent)
       transcat-hl-segment-extent
       (move-overlay transcat-hl-segment-extent
		             (transcat-hl-segment-begin-pos)
		             (transcat-hl-segment-end-pos)
		             (current-buffer))))

(provide 'transcat-hl-segment)
;; Local Variables:
;; indent-tabs-mode: nil
;; End:
;;; transcat-hl-segment.el ends here
