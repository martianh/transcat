;;; transcat-segment.el --- Segmentation functions for transcat  -*- lexical-binding: t; -*-

;; Author: Marty Hiatt <martianhiatus AT riseup.net>
;; Copyright (C) 2022 Marty Hiatt <martianhiatus AT riseup.net>
;; Package-Requires: ((emacs "28.1"))
;; Package-Version: 0.1
;; Homepage: https://codeberg.org/martianh/transcat
;; Keywords: convenience, translation, languages

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Segmentation of buffers for transcat projects.

;;; Code:

(require 'cl-lib)

(defvar transcat-segment-separator-string)
(defvar transcat-source-codes-list)
(defvar transcat-sentence-end-base)
(defvar transcat-project-source-language-code)
(defvar transcat-project-target-language-code)
(defvar transcat-forward-sentence-function)
(defvar transcat-backward-sentence-function)
(defvar transcat-src-mode)
(defvar transcat-segment-source-structs nil)

(autoload 'transcat--move-past-separator-maybe "transcat")

(defgroup transcat-segment nil
  "Transcat translation package, segmentation."
  :group 'transcat)

;; TODO: find a char with some whitespace padding
(defcustom transcat-segment-separator-string "|" ;"݊" caused problems w point
  "String that separates translation segments.
Using \"\n\n\" is not recommended as that is a paragraph break."
  :type 'string)


;;; SEGMENT PAIR STRUCT

(cl-defstruct (transcat-segment-pair
               (:constructor transcat-segment-pair-create))
  "The data for a single pair of source/target segments.
For convenience, the form should be parallel to that of an XLF file."
  id source-lang source-text target-lang target-text)


;;; SEGMENTING OUR OWN FILES:

(defun transcat-segment-set-project-source-lang ()
  "Prompt for the project's source language.
Set `transcat-project-source-language-code' as two-letter code."
  (interactive)
  (let ((response (completing-read "Source language: "
                                   transcat-source-codes-list
                                   nil 'confirm)))
    (setq transcat-project-source-language-code
          (car
           (alist-get response transcat-source-codes-list nil nil #'equal)))))

;; NB: these work before we have inserted segment separators!
;; not for interactive use with segmented buffers
(defun transcat-segment--forward-sentence ()
  "Call `transcat-forward-sentence-function'."
  (apply transcat-forward-sentence-function '()))

(defun transcat-segment--backward-sentence ()
  "Call `transcat-backward-sentence-function'."
  (apply transcat-backward-sentence-function '()))

(defun transcat-segment--get-sentence-begin ()
  "Return the beginning of the sentence at point.
Move past `transcat-separator-string' if found."
  (save-excursion
    (unless (= (point) (point-max))
      (forward-char))
    (transcat-segment--backward-sentence)
    (transcat--move-past-separator-maybe)
    (point)))

(defun transcat-segment--get-sentence-end ()
  "Return the end of the sentence at point."
  (save-excursion
    (unless (= (point) (point-max))
      (forward-char))
    (transcat-segment--backward-sentence)
    (transcat-segment--forward-sentence)
    (point)))

(defun transcat-segment--get-sentence-text ()
  "Return the text of the sentence at point."
  (buffer-substring-no-properties (transcat-segment--get-sentence-begin)
                                  (transcat-segment--get-sentence-end)))

(defun transcat-segment-source-buffer ()
  "Segment a source buffer."
  (cond ((equal major-mode 'org-mode)
         ;; TODO: handle segmentation of org elements/files
         (transcat-segment-source-buffer-org))
        ;; markdown also?
        (t
         (transcat-segment-source-buffer-text))))

(defun transcat-segment-source-buffer-text ()
  "Segment a source text buffer as a text file."
  ;; look at `ogt-segment-project'
  (interactive)
  ;; if we've already segmented:
  (when (re-search-forward transcat-segment-separator-string nil t)
    (if (not (y-or-n-p "Buffer already contains segmentation separators. Remove and re-segment?"))
        (message "Not re-segmenting.")
      (transcat-segment-unsegment-buffer)))
  (let ((sentence-end-base transcat-sentence-end-base)
        ;; (source-lang (or transcat-project-source-language-code
        ;;                  (transcat-set-project-source-lang)))
        (count 1))
    (read-only-mode -1)
    (save-excursion
      (goto-char (point-min))
      (while (transcat-segment-while-test)
        ;; TODO: handle non-sentence segmentation
        ;; see `ogt-segmentation-strategy'
        (let* ((segment-begin (transcat-segment--get-sentence-begin))
               (segment-end (transcat-segment--get-sentence-end))
               (sentence-text (transcat-segment--get-sentence-text))
               ;; build structs into a list like XLF
               ;; needed because we plan to write target text to them
               (struct
                (transcat-segment--build-struct-from-buffer count
                                                            sentence-text)))
          (add-text-properties segment-begin
                               segment-end
                               ;; TODO: either make like XLF inserting or just add
                               ;; struct in both cases
                               ;; NB: removing needs to match this also
                               (list 'id count
                                     'struct struct))
          (goto-char segment-end)
          ;; will poss leave point on paragraph break or after trailing
          ;; whitespace at para end:
          (forward-whitespace 1)
          (while (looking-at "[ ]?*\n")
            (forward-whitespace 1))
          (transcat-segment--insert-segment-separator)
          (transcat-segment--forward-sentence)
          (push struct transcat-segment-source-structs)
          (setq count (1+ count)))))
    (setq transcat-segment-source-structs
          (reverse transcat-segment-source-structs))
    (message "Buffer segmented.")
    (read-only-mode)))

(defun transcat-segment-while-test ()
  "Return t if we should continue inserting separators."
  ;; handle final empty line at buffer end:
  (if (looking-at "[ ]?*\n")
      (save-excursion
        (forward-whitespace 1)
        (not (= (point) (point-max))))
    (not (= (point) (point-max)))))

(defun transcat-segment--insert-segment-separator ()
  "Insert propertized segment separator."
  (insert (propertize transcat-segment-separator-string
                      'separator t
                      'face 'font-lock-comment-face)))

(defun transcat-segment-unsegment-buffer ()
  "Remove all segmentation separators and text properties from current buffer."
  (interactive)
  ;; (if (save-excursion
  ;; (goto-char (point-min))
  ;; (re-search-forward transcat-segment-separator-string nil t))
  ;; check if we are actually segmented
  (if (not (save-excursion
             (goto-char (point-min))
             (re-search-forward transcat-segment-separator-string nil t)))
      (message "Looks like buffer not segemented?")
    (read-only-mode -1)
    (transcat-segment--remove-segment-separator-in-buffer)
    (transcat-segment--remove-segmentation-properties)
    (when transcat-src-mode
      (read-only-mode 1))
    (message "Buffer unsegmented.")))

(defun transcat-segment--remove-segmentation-properties ()
  "Remove text properties added by segmentation."
  (save-excursion
    (goto-char (point-min))
    (while (transcat-segment-while-test)
      (let* ((segment-begin (transcat-segment--get-sentence-begin))
             (segment-end (transcat-segment--get-sentence-end)))
        (remove-text-properties segment-begin
                                segment-end
                                '(id nil
                                     struct nil)))
      (transcat-segment--forward-sentence))))

(defun transcat-segment--remove-segment-separator-in-buffer ()
  "Remove all instances of `transcat-segment-separator-string' in buffer."
  (save-excursion
    (goto-char (point-min))
    (save-match-data
      (while (re-search-forward transcat-segment-separator-string nil t)
        (replace-match "" nil)))))
;; 28.1:
;; (replace-regexp-in-region transcat-separator-string
;; ""
;; (point-min)
;; (point-max)))

(defun transcat-segment--build-struct-from-buffer (id sentence-text)
  "Build `transcat-segment-pair' struct from current buffer.
ID is the segment pair's id, SENTENCE-TEXT is the segment's text."
  (transcat-segment-pair-create
   :id id
   :source-lang transcat-project-source-language-code
   :source-text sentence-text
   :target-lang transcat-project-target-language-code
   ;; initialize target text as filled w source:
   :target-text sentence-text))

(provide 'transcat-segment)
;;; transcat-segment.el ends here
