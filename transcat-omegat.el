
(require 'dom)

(defvar transcat-project-root)
(defvar transcat-project-settings)

(defvar-local transcat-project-settings-file
  ;; contains src/tar langs, tokenizer, dir structure, ignore files, seg type,
  ;; etc
  (concat transcat-project-root
          "omegat.project"))


(cl-defstruct (transcat-project-settings-omegat
               (:constructor transcat-project-settings-omegat-create))
  "The settings information for a transcat project."
  ;; also contains all directory locations
  source-lang target-lang source-tokenizer target-tokenizer
  sentence-segmentation remove-tags support-default-translations
  source-dir-excludes)


(defun transcat--parse-xml-file (file)
  "Parse an XML FILE."
  ;; to use this we need to adapt our extraction functions:
  ;; (nxml-parse-file transcat-project-tmx-file))
  (with-temp-buffer
    (insert-file-contents file)
    (xml-parse-region (point-min) (point-max))))

;; parse TMX settings file
(defun transcat--parse-project-settings-file ()
  "Parse the TMX settings file for the current project."
  (transcat--parse-xml-file transcat-project-settings-file))

(defun transcat--get-single-project-setting (dom tag)
  "Return a single project setting TAG from parsed settings file DOM."
  (dom-text (dom-by-tag dom tag)))

(defun transcat--get-project-excludes (dom)
  "Get project excludes from parsed settings file DOM."
  (let ((list (split-string (dom-texts (dom-by-tag
                                        dom 'source_dir_excludes)
                                       "")
                            "\n")))
    (mapcar (lambda (x)
              (string-trim x))
            list)))

(defun transcat--get-project-settings ()
  "Get settings from an OmegaT project file.
Returns a struct."
  (let ((settings (transcat--parse-project-settings-file)))
    (setq transcat-project-settings
          (transcat-project-settings-omegat-create
           :source-lang (transcat--get-single-project-setting
                         settings 'source_lang)
           :target-lang (transcat--get-single-project-setting
                         settings 'target_lang)
           :source-tokenizer (transcat--get-single-project-setting
                              settings 'source-tok)
           :target-tokenizer (transcat--get-single-project-setting
                              settings 'target-tok)
           :sentence-segmentation (transcat--get-single-project-setting
                                   settings 'sentence-segmentation)
           :remove-tags (transcat--get-single-project-setting
                         settings 'remove-tags)
           :source-dir-excludes (transcat--get-project-excludes settings)
           :support-default-translations
           (transcat--get-single-project-setting
            settings 'support-default-translations)))))
