;;; transcat-langs.el --- Language code alists for transcat -*- lexical-binding: t; -*-

;; Author: Marty Hiatt <martianhiatus AT riseup.net>
;; Copyright (C) 2022 Marty Hiatt <martianhiatus AT riseup.net>
;; Package-Requires: ((emacs "28.1"))
;; Package-Version: 0.1
;; Homepage: https://codeberg.org/martianh/transcat
;; Keywords: convenience, translation, languages

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

(defconst transcat-languages-alist-omegat
  ;; Language lists returned by `segment-convert--get-ruleset-languages'
  '(("Catalan" "ca")
    ("Czech" "cz")
    ("German" "de")
    ("English" "en")
    ("Spanish" "es")
    ("Finnish" "fi")
    ("French" "fr")
    ("Italian" "it")
    ("Japanese" "ja")
    ("Dutch" "nl")
    ("Polish" "pl")
    ("Russian" "ru")
    ("Swedish" "sv")
    ("Slovak" "sk")
    ("Chinese" "zh")
    ("Default" "default")
    ("Text" "text")
    ("HTML" "html")))

(defconst transcat-languages-alist-okapi-plain
  '(("Default" "default")
    ("Thai" "th")
    ("Khmer" "km")))

(defconst transcat-languages-alist-okapi-alt
  ;; Language lists returned by `segment-convert--get-ruleset-languages'
  '(("Polish" "pl")
    ("English" "en")
    ("Romanian" "ro")
    ("Dutch" "nl")
    ("Slovak" "sk")
    ("Icelandic" "is")
    ("Russian" "ru")
    ("Default" "default")
    ("ByLineBreak" "line break")
    ("ByTwoLineBreaks" "double line break")
    ("Slovenian" "sl")))

(defconst transcat-languages-alist-okapi-icu4j
  ;; Language lists returned by `segment-convert--get-ruleset-languages'
  '(("English" "en")
    ("Spanish" "es")
    ("French" "fr")
    ("Russian" "ru")
    ("Dutch" "nl")
    ("Portuguese" "pt")
    ("Italian" "it")
    ("German" "de")
    ("Default" "default")))

(defconst transcat-language-codes-full-alist
  '(("Afrikaans" "af")
    ("Albanian" "sq")
    ("Amharic" "am")
    ("Arabic" "ar")
    ("Armenian" "hy")
    ("Azerbaijani" "az")
    ("Basque" "eu")
    ("Belarusian" "be")
    ("Bengali" "bn")
    ("Bosnian" "bs")
    ("Bulgarian" "bg")
    ("Catalan" "ca")
    ("Cebuano" "ceb")
    ("Chichewa" "ny")
    ("Chinese Simplified" "zh-CN")
    ("Chinese Traditional" "zh-TW")
    ("Corsican" "co")
    ("Croatian" "hr")
    ("Czech" "cs")
    ("Danish" "da")
    ("Dutch" "nl")
    ("English" "en")
    ("Esperanto" "eo")
    ("Estonian" "et")
    ("Filipino" "tl")
    ("Finnish" "fi")
    ("French" "fr")
    ("Frisian" "fy")
    ("Galician" "gl")
    ("Georgian" "ka")
    ("German" "de")
    ("Greek" "el")
    ("Gujarati" "gu")
    ("Haitian Creole" "ht")
    ("Hausa" "ha")
    ("Hawaiian" "haw")
    ("Hebrew" "iw")
    ("Hindi" "hi")
    ("Hmong" "hmn")
    ("Hungarian" "hu")
    ("Icelandic" "is")
    ("Igbo" "ig")
    ("Indonesian" "id")
    ("Irish" "ga")
    ("Italian" "it")
    ("Japanese" "ja")
    ("Javanese" "jw")
    ("Kannada" "kn")
    ("Kazakh" "kk")
    ("Khmer" "km")
    ("Korean" "ko")
    ("Kurdish (Kurmanji)" "ku")
    ("Kyrgyz" "ky")
    ("Lao" "lo")
    ("Latin" "la")
    ("Latvian" "lv")
    ("Lithuanian" "lt")
    ("Luxembourgish" "lb")
    ("Macedonian" "mk")
    ("Malagasy" "mg")
    ("Malay" "ms")
    ("Malayalam" "ml")
    ("Maltese" "mt")
    ("Maori" "mi")
    ("Marathi" "mr")
    ("Mongolian" "mn")
    ("Myanmar (Burmese)" "my")
    ("Nepali" "ne")
    ("Norwegian" "no")
    ("Pashto" "ps")
    ("Persian" "fa")
    ("Polish" "pl")
    ("Portuguese" "pt")
    ("Punjabi" "pa")
    ("Romanian" "ro")
    ("Russian" "ru")
    ("Samoan" "sm")
    ("Scots Gaelic" "gd")
    ("Serbian" "sr")
    ("Sesotho" "st")
    ("Shona" "sn")
    ("Sindhi" "sd")
    ("Sinhala" "si")
    ("Slovak" "sk")
    ("Slovenian" "sl")
    ("Somali" "so")
    ("Spanish" "es")
    ("Sundanese" "su")
    ("Swahili" "sw")
    ("Swedish" "sv")
    ("Tajik" "tg")
    ("Tamil" "ta")
    ("Telugu" "te")
    ("Thai" "th")
    ("Turkish" "tr")
    ("Ukrainian" "uk")
    ("Urdu" "ur")
    ("Uzbek" "uz")
    ("Vietnamese" "vi")
    ("Welsh" "cy")
    ("Xhosa" "xh")
    ("Yiddish" "yi")
    ("Yoruba" "yo")
    ("Zulu" "zu")))
