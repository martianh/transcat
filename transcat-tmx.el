;;; transcat-tmx.el --- Work with translation memories (TMX files)  -*- lexical-binding: t; -*-

;; Author: Marty Hiatt <martianhiatus AT riseup.net>
;; Copyright (C) 2022 Marty Hiatt <martianhiatus AT riseup.net>
;; Package-Requires: ((emacs "28.1"))
;; Package-Version: 0.1
;; Homepage: https://codeberg.org/martianh/transcat
;; Keywords: convenience, translation, languages

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Work with translation memories (TMX files) in Emacs.
;; For now, projects are assumed to be structured as OmegaT projects.

;;; Code:

(require 'xml)
(require 'nxml-parse)
(require 'dom)
(require 'cl-lib)
(require 'transcat)

(defvar transcat-tmx-data nil)

(defvar transcat-tmx-tmx-file-base nil)

(defvar transcat-tmx-file
  (concat transcat-project-root
          "/tm/" transcat-project-tmx-file-base))

;;; structs
(cl-defstruct (transcat-tmx-segment-pair (:constructor transcat-tmx-segment-pair-create))
  "A pair of source and target translation segments."
  source target)

(cl-defstruct (transcat-tmx-segment (:constructor transcat-tmx-segment-create))
  "The data for a single translation segment."
  language creation-id creation-date change-id change-date segment)

(cl-defstruct (transcat-tmx-header (:constructor transcat-header-create))
  "The TMX file's header information."
  creation-tool o-tmf data-type segment-type source)

;; parse TMX segmentation file
(defun transcat-tmx-parse-tmx-file ()
  "Parse TMX file for the current project."
  (transcat--parse-xml-file transcat-tmx-file))

;; tmx segmented data
(defun transcat-tmx--get-tmx-data (tmx)
  "Parse and convert TMX XML data and set to `transcat-tmx-data'."
  (setq transcat-tmx-data
        `(:header ,(transcat-tmx--get-header tmx)
                  :body ,(transcat-tmx--map-segment-pairs tmx))))

(defun transcat-tmx--get-header (tmx)
  "Get header information from TMX."
  (let ((header (dom-by-tag (car tmx) 'header)))
    (transcat-header-create
     :source (dom-attr header 'srclang)
     :segment-type (dom-attr header 'segtype)
     :data-type (dom-attr header 'datatype)
     :o-tmf (dom-attr header 'o-tmf)
     :creation-tool (dom-attr header 'creationtool))))

(defun transcat-tmx--get-body (tmx)
  "Get the body data for TMX."
  (car (dom-by-tag (car tmx) 'body)))

(defun transcat-tmx--map-segment-pairs (tmx)
  "Collect the segment pairs for TMX."
  (let* ((body (transcat-tmx--get-body tmx))
         (children (dom-children body)))
    (remove nil (mapcar (lambda (x)
                          (when (listp x)
                            (transcat-tmx--get-segment-pair x)))
                        children))))

(defun transcat-tmx--get-segment-pair (pair)
  "Create a single segment pair from PAIR."
  (transcat-tmx-segment-pair-create
   :source (transcat-tmx--get-segment
            (nth 1 (dom-children pair)))
   :target (transcat-tmx--get-segment
            (nth 3 (dom-children pair)))))

(defun transcat-tmx--get-segment (seg)
  "Create a single segment from SEG."
  (transcat-tmx-segment-create
   :segment (dom-text (dom-by-tag seg 'seg))
   :language (dom-attr seg 'xml:lang) ; en-GB
   ;; NB: these are target only:
   :creation-id (dom-attr seg 'creationid) ; who/what created seg
   :creation-date (dom-attr seg 'creationdate) ; yyyymmddThhmmssZ
   :change-id (dom-attr seg 'changeid) ; who last changed seg
   :change-date (dom-attr seg 'changedate))) ; yyyymmddThhmmssZ

(provide 'transcat-tmx)
;;; transcat-tmx.el ends here
